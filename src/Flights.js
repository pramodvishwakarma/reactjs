import React from 'react';
import './App.css';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import RoundTrip from "./RoundTrip";
import OneWay from "./OnewayTrip";
import MultiCity from "./MultiCityTrip";



function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={0}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    'aria-controls': `nav-tabpanel-${index}`,
  };
}

function LinkTab(props) {
  return (
    <Tab
      component="a"
      onClick={event => {
        event.preventDefault();
      }}
      {...props}
    />
  );
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  tabBtn:{
  width: '80px',
  height:'20px',
  minHeight:'25px',
  display: 'flex',
  border:'1px solid #e4e4e4',
  borderRadius:'10px',
  color:'#333333',
  marginRight:'10px',
  whiteSpace: 'nowrap',
  },
 
}));

export default function FlightSearches() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <div className={classes.root}>
      <AppBar position="static" id="tabStyle">
        <Tabs
          variant="fullWidth"
          value={value}
          onChange={handleChange}
        >
          <LinkTab label="Round Trip" className={classes.tabBtn} href="/drafts" {...a11yProps(0)} />
          <LinkTab label="One Way" className={classes.tabBtn} href="/trash" {...a11yProps(1)} />
          <LinkTab label="Multi City" className={classes.tabBtn} href="/spam" {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
     <RoundTrip/>
      </TabPanel>
      <TabPanel value={value} index={1}>
      <OneWay/>
      </TabPanel>
      <TabPanel value={value} index={2}>
      <MultiCity/>
      </TabPanel>
    </div>
  );
}
