import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import Button from '@material-ui/core/Button';
import {MuiPickersUtilsProvider,KeyboardDatePicker,} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import 'date-fns';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  formControl: { //radio button
    margin: theme.spacing(3),
    padding:'0',//radio button padding
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    // flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
    display:"flex-end"
  },
  collapseBox:{
    boxShadow:'none',
  }
}));
export default function HotelSearches() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = panel => (_event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };
  const [selectedDate, setSelectedDate] = React.useState(new Date('2014-08-18T21:11:54'));

  const handleDateChange = date => {
    setSelectedDate(date);
  };
  return (
    <React.Fragment>
      <Grid container spacing={3} >
        <Grid item xs={12} sm={12}>
          <TextField
            required
            id="hotels"
            name="hotels"
            label="Select City, Location or Hotel Name (Worldwide)"
            fullWidth
            autoComplete="hotels"
            value="Goa, Goa, India"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
         <Grid container justify="space-around">
        <KeyboardDatePicker
        autoOk
        variant="inline"
        inputVariant="outlined"
        margin="normal"
        label="Depart Date"
        format="dd/mm/yyyy"
        value={selectedDate}
        InputAdornmentProps={{ position: "end" }}
        onChange={date => handleDateChange(date)}
        minDate={new Date()}        
      />
        </Grid>
    </MuiPickersUtilsProvider>
        </Grid>
        <Grid item xs={12} sm={6}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
         <Grid container justify="space-around">
         <KeyboardDatePicker
         autoOk
        variant="inline"
        inputVariant="outlined"
        margin="normal"
        label="Return Date"
        format="dd/mm/yyyy"
        value={selectedDate}
        InputAdornmentProps={{ position: "end" }}
        onChange={date => handleDateChange(date)}
        disabled
      />
        </Grid>
    </MuiPickersUtilsProvider>
        </Grid>
        <Grid item xs={12} >
        <div className={classes.root}>
      <ExpansionPanel expanded={expanded === 'panel1'} onChange={handleChange('panel1')} className={classes.collapseBox}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <Typography className={classes.heading}>Traveller(s), Room</Typography>
          {/* <Typography className={classes.secondaryHeading}>I am an expansion panel</Typography> */}
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <React.Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
        <TextField
          id="outlined-number"
          label="Adult"
          type="number"
          InputLabelProps={{
            shrink: true,
          }}
          variant="outlined"
        required/>
        </Grid>
        <Grid item xs={12} sm={6}>
        <TextField
          id="outlined-number"
          label="Child"
          type="number"
          InputLabelProps={{
            shrink: true,
          }}
          variant="outlined"
        required/>
        </Grid>
        <Grid item xs={12} sm={5}>
        <Button
        variant="contained"
        color="primary"
        className={classes.button}
      >
        Add Room
      </Button>
        </Grid>
        </Grid>
        </React.Fragment>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
        </Grid>
        <Grid item xs={12} sm={7}>
        <FormControlLabel
        control={<Checkbox color="secondary" name="saveDetails" value="yes" />}
        label="Non Stop Flights"/>
        </Grid>
        <Grid item xs={12} sm={5}>
        <Button
        variant="contained"
        color="primary"
        className={classes.button}
        endIcon={<ArrowRightAltIcon/>}
      >
        Search Hotels
      </Button>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}