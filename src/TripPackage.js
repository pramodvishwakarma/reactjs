import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    wrap:'nowrap',
  },
  inline: {
    display: 'block',
    wrap:'nowrap',
  },
}));

export default function TripPackage() {
  const classes = useStyles();

  return (
    <Grid container spacing={3}>
    <Grid item xs={12} sm={6} md={6} >
    <h3  noWrap>Top International Holiday Destinations</h3>
    <List className={classes.root}>
      <ListItem button>
        <ListItemAvatar>
          <Avatar>
            <img src="/assets/europe.png" alt="europe"/>
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Europe" secondary={
            <React.Fragment>
              <Typography component="span" variant="body2" className={classes.inline} color="textPrimary">
                Rs 119,990.00/ person </Typography>
              {"6 Nights/ 7 Days "}
            </React.Fragment>
          }/>
        <IconButton aria-label="delete" className={classes.margin} size="small">
          <ArrowForwardIcon fontSize="inherit" />
        </IconButton>
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem button>
        <ListItemAvatar>
          <Avatar>
            <img src="/assets/nz.png" alt="new zealand" />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="New Zealand" secondary={
            <React.Fragment>
              <Typography component="span" variant="body2" className={classes.inline} color="textPrimary">
                Rs 119,990.00/ person </Typography>
              {"6 Nights/ 7 Days "}
            </React.Fragment>
          }/>
        <IconButton aria-label="delete" className={classes.margin} size="small">
          <ArrowForwardIcon fontSize="inherit" />
        </IconButton>
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem button>
        <ListItemAvatar>
          <Avatar>
          <img src="/assets/singa.png" alt="sigapore" />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Singapore" secondary={
            <React.Fragment>
              <Typography component="span" variant="body2" className={classes.inline} color="textPrimary">
                Rs 24,000.00/ person </Typography>
              {"2 Nights/ 3 Days "}
            </React.Fragment>
          }/>
        <IconButton aria-label="delete" className={classes.margin} size="small">
          <ArrowForwardIcon fontSize="inherit" />
        </IconButton>
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem button>
        <ListItemAvatar>
          <Avatar>
            <img src="/assets/1.png" alt="Russia" />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Russia" secondary={
            <React.Fragment>
              <Typography component="span" variant="body2" className={classes.inline} color="textPrimary">
                Rs 74,990.00/ person </Typography>
              {"6 Nights/ 7 Days "}
            </React.Fragment>
          }/>
        <IconButton aria-label="delete" className={classes.margin} size="small">
          <ArrowForwardIcon fontSize="inherit" />
        </IconButton>
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem button>
        <ListItemAvatar>
          <Avatar>
          <img src="/assets/2.png" alt="ICC Women World Cup" />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="ICC Women World Cup" secondary={
            <React.Fragment>
              <Typography component="span" variant="body2" className={classes.inline} color="textPrimary">
                Rs 28,990.00/ person </Typography>
              {"3 Nights/ 4 Days "}
            </React.Fragment>
          }/>
        <IconButton aria-label="delete" className={classes.margin} size="small">
          <ArrowForwardIcon fontSize="inherit" />
        </IconButton>
      </ListItem>
    </List>
    </Grid>
    <Grid item xs={12} sm={6} md={6}>
    <h3 noWrap>Top Domestic Holiday Destinations</h3>
    <List className={classes.root}>
      <ListItem button>
        <ListItemAvatar>
          <Avatar>
            <img src="/assets/3.jpg" alt="sri lanka"/>
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Sri Lanka" noWrap secondary={
            <React.Fragment>
              <Typography component="span" variant="body2" className={classes.inline} color="textPrimary">
                Rs 18,999.00/ person </Typography>
              {"4 Nights/ 5 Days "}
            </React.Fragment>
          }/>
        <IconButton aria-label="delete" className={classes.margin} size="small">
          <ArrowForwardIcon fontSize="inherit" />
        </IconButton>
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem button>
        <ListItemAvatar>
          <Avatar>
            <img src="/assets/4.jpg" alt="Kerala"/>
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Kerala" secondary={
            <React.Fragment>
              <Typography component="span" variant="body2" className={classes.inline}  color="textPrimary">
                Rs 12,999.00/ person </Typography>
              {"4 Nights/ 5 Days "}
            </React.Fragment>
          }/>
        <IconButton aria-label="delete" className={classes.margin} size="small">
          <ArrowForwardIcon fontSize="inherit" />
        </IconButton>
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem button>
        <ListItemAvatar>
          <Avatar>
            <img src="/assets/5.png" alt="Ladakh"/>
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Ladakh" secondary={
            <React.Fragment>
              <Typography component="span" variant="body2" className={classes.inline} color="textPrimary">
                Rs 17,999.00/ person </Typography>
              {"4 Nights/ 5 Days "}
            </React.Fragment>
          }/>
        <IconButton aria-label="delete" className={classes.margin} size="small">
          <ArrowForwardIcon fontSize="inherit" />
        </IconButton>
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem button>
        <ListItemAvatar>
          <Avatar>
            <img src="/assets/6.jpg" alt="Golden Temple"/>
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Golden Temple" secondary={
            <React.Fragment>
              <Typography component="span" variant="body2" className={classes.inline} color="textPrimary">
                Rs 17,999.00/ person </Typography>
              {"5 Nights/ 6 Days "}
            </React.Fragment>
          }/>
        <IconButton aria-label="delete" className={classes.margin} size="small">
          <ArrowForwardIcon fontSize="inherit" />
        </IconButton>
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem button>
        <ListItemAvatar>
          <Avatar>
            <img src="/assets/7.jpg" alt="Himanchal"/>
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Himanchal" secondary={
            <React.Fragment>
              <Typography component="span" variant="body2" className={classes.inline} color="textPrimary">
                Rs 7,499.00/ person </Typography>
              {"4 Nights/ 5 Days "}
            </React.Fragment>
          }/>
        <IconButton aria-label="delete" className={classes.margin} size="small">
          <ArrowForwardIcon fontSize="inherit" />
        </IconButton>
      </ListItem>
    </List>
    </Grid>
    </Grid>
    
  );
}