import React from 'react';
import './App.css';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import FlightIcon from '@material-ui/icons/Flight';
import HotelIcon from '@material-ui/icons/Hotel';
import DirectionsWalkIcon from '@material-ui/icons/DirectionsWalk';
import DirectionsCarIcon from '@material-ui/icons/DirectionsCar';
import SecurityIcon from '@material-ui/icons/Security';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import FlightSearches from "./Flights";
import HotelSearches from "./Hotels";
import HolidaySearches from "./Holidays";


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};


function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    'aria-controls': `scrollable-force-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  headerStyle:{
    backgroundColor:'transparent',
    boxShadow:'none',
  },
  tabHandel:{
    display:'nowrap',
  },
  tabStyle:{
  minwidth: '50px',
  minHeight:'50px',
  padding:'10px',
  display: 'flex',
  border:'1px solid #e4e4e4',
  borderRadius:'50% ',
  color:'#666',
  marginRight:'10px',
  whiteSpace: 'nowrap',
  }
  
}));

export default function ScrollableTabsButtonForce() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.headerStyle}>
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
        className={classes.tabHandel}>
          <Tab label="Flight" className={classes.tabStyle} icon={<FlightIcon />} {...a11yProps(0)}/>
          <Tab label="Hotel" className={classes.tabStyle} icon={<HotelIcon />} {...a11yProps(1)} />
          <Tab label="Holiday" className={classes.tabStyle} icon={<DirectionsWalkIcon />} {...a11yProps(2)} />
          <Tab label="Car" disabled className={classes.tabStyle} icon={<DirectionsCarIcon />} {...a11yProps(3)} />
          <Tab label="Insur." disabled className={classes.tabStyle} icon={<SecurityIcon />} {...a11yProps(4)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
       <FlightSearches/>
      </TabPanel>
      <TabPanel value={value} index={1}>
       <HotelSearches/>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <HolidaySearches/>
      </TabPanel>
      <TabPanel value={value} index={3}>
        Item Four
      </TabPanel>
      <TabPanel value={value} index={4}>
        Item Five
      </TabPanel>
      
    </div>
  );
}
