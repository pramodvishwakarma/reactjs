import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
    slidesToSlide: 3, // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    slidesToSlide: 2, // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1, // optional, default to 1.
  },
  
};

const useStyles = makeStyles({
  card: {
    minWidth: 180,
    wrap:'nowrap',
    },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
    wrap:'nowrap',
  },
  title: {
    fontSize: 14,
    wrap:'nowrap',
  },
  pos: {
    marginBottom: 12,
    wrap:'nowrap',
  },
  destination:{
    marginTop:10,
    wrap:'nowrap',
    
  },
  price_btn:{
    marginLeft:8,
    wrap:'nowrap',
  },
  carouselItem:{
    padding:'16px',
  }
});

export default function ItemCarousel() {
  const classes = useStyles();
  return (
<Carousel responsive={responsive}
autoPlay
slidesToSlide={1}
autoPlaySpeed={7000}
transitionDuration={500}
// removeArrowOnDeviceType={["tablet", "mobile"]}
itemClass="carousel-item-padding-40-px"
 className={classes.carouselItem}>
    <Card className={classes.card} variant="outlined">
      <CardContent>
      <div className="origin_destination">
          <div className="elip">Delhi</div>
           <div className="time">
               <Typography className={classes.title} color="textSecondary" gutterBottom>
            Fri, 13 Mar
        </Typography></div>
            <div className="elip">Mumbai</div>
            </div>
            <Typography variant="body2" component="p" className={classes.destination}>
          Start From
        </Typography>
      </CardContent>
      <CardActions className={classes.price_btn} >
        <Button size="small" variant="contained" color="secondary" >Rs 2,320</Button>
      </CardActions>
    </Card>
    <Card className={classes.card} variant="outlined">
      <CardContent>
      <div className="origin_destination">
          <div className="elip">Delhi</div>
           <div className="time">
               <Typography className={classes.title} color="textSecondary" gutterBottom>
            Fri, 13 Mar
        </Typography></div>
            <div className="elip">Mumbai</div>
            </div>
            <Typography variant="body2" component="p" className={classes.destination}>
          Start From
        </Typography>
      </CardContent>
      <CardActions className={classes.price_btn} >
        <Button size="small" variant="contained" color="secondary" >Rs 2,320</Button>
      </CardActions>
    </Card>
    <Card className={classes.card} variant="outlined">
      <CardContent>
      <div className="origin_destination">
          <div className="elip">Delhi</div>
           <div className="time">
               <Typography className={classes.title} color="textSecondary" gutterBottom>
            Fri, 13 Mar
        </Typography></div>
            <div className="elip">Mumbai</div>
            </div>
            <Typography variant="body2" component="p" className={classes.destination}>
          Start From
        </Typography>
      </CardContent>
      <CardActions className={classes.price_btn} >
        <Button size="small" variant="contained" color="secondary" >Rs 2,320</Button>
      </CardActions>
    </Card>
    <Card className={classes.card} variant="outlined">
      <CardContent>
      <div className="origin_destination">
          <div className="elip">Delhi</div>
           <div className="time">
               <Typography className={classes.title} color="textSecondary" gutterBottom>
            Fri, 13 Mar
        </Typography></div>
            <div className="elip">Mumbai</div>
            </div>
            <Typography variant="body2" component="p" className={classes.destination}>
          Start From
        </Typography>
      </CardContent>
      <CardActions className={classes.price_btn} >
        <Button size="small" variant="contained" color="secondary" >Rs 2,320</Button>
      </CardActions>
    </Card>
    <Card className={classes.card} variant="outlined">
      <CardContent>
      <div className="origin_destination">
          <div className="elip">Delhi</div>
           <div className="time">
               <Typography className={classes.title} color="textSecondary" gutterBottom>
            Fri, 13 Mar
        </Typography></div>
            <div className="elip">Mumbai</div>
            </div>
            <Typography variant="body2" component="p" className={classes.destination}>
          Start From
        </Typography>
      </CardContent>
      <CardActions className={classes.price_btn} >
        <Button size="small" variant="contained" color="secondary" >Rs 2,320</Button>
      </CardActions>
    </Card>
    <Card className={classes.card} variant="outlined">
      <CardContent>
      <div className="origin_destination">
          <div className="elip">Delhi</div>
           <div className="time">
               <Typography className={classes.title} color="textSecondary" gutterBottom>
            Fri, 13 Mar
        </Typography></div>
            <div className="elip">Mumbai</div>
            </div>
            <Typography variant="body2" component="p" className={classes.destination}>
          Start From
        </Typography>
      </CardContent>
      <CardActions className={classes.price_btn} >
        <Button size="small" variant="contained" color="secondary" >Rs 2,320</Button>
      </CardActions>
    </Card>
    <Card className={classes.card} variant="outlined">
      <CardContent>
      <div className="origin_destination">
          <div className="elip">Delhi</div>
           <div className="time">
               <Typography className={classes.title} color="textSecondary" gutterBottom>
            Fri, 13 Mar
        </Typography></div>
            <div className="elip">Mumbai</div>
            </div>
            <Typography variant="body2" component="p" className={classes.destination}>
          Start From
        </Typography>
      </CardContent>
      <CardActions className={classes.price_btn} >
        <Button size="small" variant="contained" color="secondary" >Rs 2,320</Button>
      </CardActions>
    </Card>
    <Card className={classes.card} variant="outlined">
      <CardContent>
      <div className="origin_destination">
          <div className="elip">Delhi</div>
           <div className="time">
               <Typography className={classes.title} color="textSecondary" gutterBottom>
            Fri, 13 Mar
        </Typography></div>
            <div className="elip">Mumbai</div>
            </div>
            <Typography variant="body2" component="p" className={classes.destination}>
          Start From
        </Typography>
      </CardContent>
      <CardActions className={classes.price_btn} >
        <Button size="small" variant="contained" color="secondary" >Rs 2,320</Button>
      </CardActions>
    </Card>
    </Carousel>
  );
}
