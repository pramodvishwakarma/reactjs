import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 400,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  flex:{
    flexDirection:'end',
  }
  
}));
export default function HolidaySearches() {
  const classes = useStyles();
  const [Month, setMonth] = React.useState('');
  const handleChange = event => {
    setMonth(event.target.value);
  };
  return (
    <React.Fragment>
      <Grid container spacing={3} >
      <Grid item xs={12} sm={6}>
          <TextField
            required
            id="departfrom"
            name="departFrom"
            label="Depart From"
            fullWidth
            autoComplete="Departfrom"
            value="New Delhi"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="goingto"
            name="goingTo"
            label="Going To"
            fullWidth
            autoComplete="goingto"
            value="Abu Dhabi"
          />
        </Grid>
        <Grid item xs={12}>
        <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-label">Select of Travel (Optional)</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={Month}
          onChange={handleChange}
        >
          <MenuItem value={1}>December 2019</MenuItem>
          <MenuItem value={2}>January 2020</MenuItem>
          <MenuItem value={3}>February 2020</MenuItem>
          <MenuItem value={4}>March 2020</MenuItem>
          <MenuItem value={5}>April 2020</MenuItem>
          <MenuItem value={6}>May 2020</MenuItem>
          <MenuItem value={7}>June 2020</MenuItem>
        </Select>
      </FormControl>
        </Grid>
        <Grid item xs={12} sm={5} justifyContent="flex-end" >
        <Button
        variant="contained"
        color="primary"
        size="small"
        className={classes.button}
        endIcon={<ArrowRightAltIcon/>}
      >
        Search Holidays
      </Button>
        </Grid>
      </Grid>
    </React.Fragment>
  );
  }