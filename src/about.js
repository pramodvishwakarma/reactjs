import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: 'auto',
    marginTop:'20px',
    maxWidth: 800,
  },
  image: {
    width: 200,
    height: 200,
    alignItems:"flex-end",
  },
  app:{
    width:'100px',
    height:'50px',
  },
  appGoogle:{
    width:'100px',
    height:'30px',
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
    alignItems:"flex-end",
    
  },
  margin: {
    margin:'6px',
  },
}));

export default function About() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Grid container spacing={7}>
        <Grid item>
            <Grid item xs direction="row" spacing={2} wrap="nowrap">
              <Grid item xs>
                <Typography gutterBottom variant="subtitle1" wrap="nowrap" spacing={2}>
                Why book with MileHigh.com?
                </Typography>
                <Typography variant="body2" gutterBottom wrap="nowrap">
                7 Brilliant reasons Yatra should be your one-stop-shop!
                </Typography>
                <Typography variant="body2" color="textSecondary">
                Book Flights, Hotels, Trains, Buses, Cruise and Holiday Packages
                </Typography>
                <Typography variant="body2" color="textSecondary">
                On MileHigh.com, you can tailor your trip from end-to-end by scouring suitable flights 
                and making your flight booking before proceeding with your hotel bookings. MileHigh’s 
                vast hotel repository will see you through this process seamlessly. Any intervening 
                journey can be conveniently planned by searching up relevant train connectivity and 
                making an IRCTC ticket booking. Look up well-researched holiday packages, sift through
                cruise packages and finalise your entire trip on just one platform.
                </Typography>
              </Grid>
              <Grid item>
              <Button size="small">Read More</Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
}
