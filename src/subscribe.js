import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: 'auto',
    marginTop:'20px',
    maxWidth: 800,
  },
  image: {
    width: 200,
    height: 200,
    alignItems:"flex-end",
  },
  app:{
    width:'100px',
    height:'50px',
  },
  appGoogle:{
    width:'100px',
    height:'30px',
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
    alignItems:"flex-end",
    
  },
  margin: {
    margin:'6px',
  },
}));

export default function Subscribe() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Grid container spacing={7}>
        <Grid item>
            <Grid item xs direction="row" spacing={2} wrap="nowrap">
              <Grid item xs>
                <Typography gutterBottom variant="subtitle1" wrap="nowrap" spacing={2}>
                Download MileHigh from your phone!
                </Typography>
                <Typography variant="body2" gutterBottom wrap="nowrap">
                Best travel deals on the go - only on MileHigh mobile!
                </Typography>
                <Typography variant="body2" color="textSecondary">
                Download via SMS
                </Typography>
              </Grid>
              <Grid item direction="row">
            <form style={{display:'flex'}}>
            <TextField
              variant="outlined"
              margin="dense"
              required
              fullWidth
              id="Mobile Number"
              label="Enter Your Mobile"
              name="Enter Mobille"
              autoComplete="false"
              type="phone"
               />
            <Button variant="contained" color="secondary" type="submit" className={classes.margin}>
            Confirm
            </Button>
           </form>
          </Grid>
              <Grid item>
                <Typography variant="body2" style={{ cursor: 'pointer' }} wrap="nowrap">
                Give a missed call on 0806 7747 755 to download the Yatra app
                </Typography>
              </Grid>
            </Grid>
            <Grid item >
            <ButtonBase className={classes.app}>
              <img className={classes.img} src="/assets/app-store.svg" alt=""/>
              </ButtonBase>
              <ButtonBase className={classes.appGoogle}>
              <img  className={classes.img} src="/assets/google-play.svg" alt=""/>
              </ButtonBase>
            </Grid>
          </Grid>
          <Grid item justify="flex-end" >
            <ButtonBase className={classes.image}>
              <img className={classes.img} alt="complex" src="https://milehighstore.s3.ap-south-1.amazonaws.com/assets/website/milehigh01.png" />
            </ButtonBase>
          </Grid>
         
        </Grid>
      </Paper>
    </div>
  );
}
